#!/usr/bin/env python3

# https://codeforces.com/problemset/problem/4/A


watermelon_weight: int = int(input())
if watermelon_weight % 2 == 0 and watermelon_weight != 2:
    print("YES")
else:
    print("NO")
