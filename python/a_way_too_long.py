#!/usr/bin/env python3

n: int = int(input())

l: list = []
for i in range(n):
    l.append(input())

for word in l:
    if len(word) > 10:
        print(f"{word[0]}{len(word) - 2}{word[-1]}")
    else:
        print(word)
